provider "aws" {
  region = "eu-west-1"
}

resource "aws_instance" "test-instance" {
  ami                         = var.ami_type
  instance_type               = var.instance_type
  subnet_id                   = var.subnet
  vpc_security_group_ids      = [var.security_group]
  associate_public_ip_address = true
  tags = {
    "Account"          = "GCE"
    "ApplicationOwner" = var.owner
    "Environment"      = var.environment
    "Name"             = var.name_of_instance
    "Product"          = "Platform"
    "ProductOwner"     = var.owner
  }
  provisioner "file" {
    source      = "atominstall64.sh"
    destination = "/tmp/atominstall64.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/atominstall64.sh",
      "/tmp/atominstall64.sh",
    ]
  }
}