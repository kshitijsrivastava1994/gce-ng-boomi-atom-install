variable "ami_type" {  
type = string      
default = "ami-0198ec889f973cc43"
}
variable "instance_type"{ 
type = string
default = "t2.xlarge"
}
variable "subnet"{  
type = string
default = "subnet-012b2daa3212e631d"
}
variable "security_group"{  
type = string      
default = "sg-0e9519a88ded649f4"
}
variable "owner"{ 
type = string      
default = "kshitij.srivastava@tenerity.com"
}
variable "environment"{ 
type = string      
default = "dev"
}
variable "name_of_instance"{
type = string      
default = "dev-gce-nextgen-boomi-atom2"
}
